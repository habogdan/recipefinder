package ro.devis.recipefinder.service;

import com.google.api.core.ApiFuture;
import com.google.cloud.firestore.*;
import com.google.firebase.cloud.FirestoreClient;
import com.google.gson.JsonObject;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ro.devis.recipefinder.model.Ingredient;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

@Service
public class IngredientService {
    public static final String COL_NAME = "ingredients";

    public String saveIngredient(Ingredient ingredient) throws InterruptedException, ExecutionException {
        Firestore dbFirestore = FirestoreClient.getFirestore();
        ingredient.setID(dbFirestore.collection(COL_NAME).document().getId());
        ApiFuture<WriteResult> collectionsApiFuture = dbFirestore.collection(COL_NAME).document(ingredient.getID()).set(ingredient);
        JsonObject response = new JsonObject();
        response.addProperty("data", collectionsApiFuture.get().getUpdateTime().toString());
        return response.toString();
    }

    public Ingredient getIngredient(String ID) throws InterruptedException, ExecutionException {
        Firestore dbFirestore = FirestoreClient.getFirestore();
        DocumentReference documentReference = dbFirestore.collection(COL_NAME).document(ID);
        ApiFuture<DocumentSnapshot> future = documentReference.get();
        DocumentSnapshot document = future.get();
        Ingredient ingredient = null;
        if (document.exists()) {
            ingredient = document.toObject(Ingredient.class);
            return ingredient;
        } else {
            return null;
        }
    }

    //TODO test if ingredient come with less fields 
    public String updateIngredient(Ingredient ingredient) throws InterruptedException, ExecutionException {
        Firestore dbFirestore = FirestoreClient.getFirestore();
        ApiFuture<WriteResult> collectionsApiFuture = dbFirestore.collection(COL_NAME).document(ingredient.getID()).set(ingredient);
        JsonObject response = new JsonObject();
        response.addProperty("data", collectionsApiFuture.get().getUpdateTime().toString());
        return response.toString();
    }

    public String deleteIngredient(String ID) {
        Firestore dbFirestore = FirestoreClient.getFirestore();
        ApiFuture<WriteResult> writeResult = dbFirestore.collection(COL_NAME).document(ID).delete();
        JsonObject response = new JsonObject();
        response.addProperty("data", "Document with Ingredient ID "+ID+" has been deleted");
        return response.toString();
    }

    public List<Ingredient> getAllIngredients() throws ExecutionException, InterruptedException {
        Firestore dbFirestore = FirestoreClient.getFirestore();
        CollectionReference documentReference = dbFirestore.collection(COL_NAME);
        ApiFuture<QuerySnapshot> future = documentReference.get();
        List<QueryDocumentSnapshot> documents = future.get().getDocuments();
        List<Ingredient> ingredientList = new ArrayList<Ingredient>();
        for (QueryDocumentSnapshot document : documents) {
            ingredientList.add(document.toObject(Ingredient.class));
        }
        return ingredientList;
    }

}
