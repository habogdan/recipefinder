package ro.devis.recipefinder.service;

import com.google.api.core.ApiFuture;
import com.google.cloud.firestore.*;
import com.google.firebase.cloud.FirestoreClient;
import com.google.gson.JsonObject;
import org.springframework.stereotype.Service;
import ro.devis.recipefinder.model.Ingredient;
import ro.devis.recipefinder.model.IngredientList;
import ro.devis.recipefinder.model.Recipe;

import java.util.*;
import java.util.concurrent.ExecutionException;

@Service
public class RecipeService {

    public static final String COL_NAME = "recipes";
    public static final String SUB_COL_NAME = "ingredientList";

    public Recipe getRecipe(String ID) throws InterruptedException, ExecutionException {
        Firestore dbFirestore = FirestoreClient.getFirestore();
        DocumentReference documentReference = dbFirestore.collection(COL_NAME).document(ID);
        ApiFuture<DocumentSnapshot> future = documentReference.get();
        DocumentSnapshot document = future.get();
        Recipe recipe = null;
        if (document.exists()) {
            recipe = document.toObject(Recipe.class);
            recipe.setIngredientList(getAllIngredientsOfRecipe(ID));
            return recipe;
        } else {
            return null;
        }
    }

    public List<IngredientList> getAllIngredientsOfRecipe(String ID) throws ExecutionException, InterruptedException {
        Firestore dbFirestore = FirestoreClient.getFirestore();
        CollectionReference documentReference = dbFirestore.collection(COL_NAME).document(ID).collection(SUB_COL_NAME);
        ApiFuture<QuerySnapshot> future = documentReference.get();
        List<QueryDocumentSnapshot> documents = future.get().getDocuments();
        List<IngredientList> ingredientList = new ArrayList<IngredientList>();
        for (QueryDocumentSnapshot document : documents) {
            ingredientList.add(document.toObject(IngredientList.class));
        }
        return ingredientList;
    }

    public List<Recipe> getAllRecipes() throws ExecutionException, InterruptedException {
        Firestore dbFirestore = FirestoreClient.getFirestore();
        CollectionReference documentReference = dbFirestore.collection(COL_NAME);
        ApiFuture<QuerySnapshot> future = documentReference.get();
        List<QueryDocumentSnapshot> documents = future.get().getDocuments();
        List<Recipe> recipeList = new ArrayList<Recipe>();
        for (QueryDocumentSnapshot document : documents) {
            Recipe recipe = null;
            recipe = document.toObject(Recipe.class);
            recipe.setIngredientList(getAllIngredientsOfRecipe(recipe.getID()));
            recipeList.add(recipe);
        }
        return recipeList;
    }

    public String saveRecipe(Recipe recipe) throws InterruptedException, ExecutionException {
        Firestore dbFirestore = FirestoreClient.getFirestore();
        recipe.setID(dbFirestore.collection(COL_NAME).document().getId());

        Map<String, Object> docData = new HashMap<>();
        docData.put("id", recipe.getID());
        docData.put("name", recipe.getName());
        docData.put("photoURL", recipe.getPhotoURL());
        docData.put("description", recipe.getDescription());
        docData.put("specific", recipe.getSpecific());
        docData.put("meal", recipe.getMeal());

        ApiFuture<WriteResult> collectionsApiFuture = dbFirestore.collection(COL_NAME).document(recipe.getID()).set(docData);

        for (int i = 0; i < recipe.getIngredientList().size(); i++) {
            saveIngredientOfList(recipe.getIngredientList().get(i), recipe.getID());
        }

        JsonObject response = new JsonObject();
        response.addProperty("data", collectionsApiFuture.get().getUpdateTime().toString());
        return response.toString();
    }

    public String saveIngredientOfList(IngredientList ingredientOFList, String ID) throws InterruptedException, ExecutionException {
        Firestore dbFirestore = FirestoreClient.getFirestore();
        ApiFuture<WriteResult> collectionsApiFuture = dbFirestore.collection(COL_NAME).document(ID)
                .collection(SUB_COL_NAME).document(ingredientOFList.getID()).set(ingredientOFList);
        JsonObject response = new JsonObject();
        response.addProperty("Successfully added IngredientOfList", collectionsApiFuture.get().getUpdateTime().toString());
        return response.toString();
    }
    
    public String deleteRecipe(String ID) throws ExecutionException, InterruptedException {
        Firestore dbFirestore = FirestoreClient.getFirestore();

        for (IngredientList i : getAllIngredientsOfRecipe(ID)){
            dbFirestore.collection(COL_NAME).document(ID).collection(SUB_COL_NAME).document(i.getID()).delete();
        }

        ApiFuture<WriteResult> writeResult2 = dbFirestore.collection(COL_NAME).document(ID).delete();
        JsonObject response = new JsonObject();
        response.addProperty("data", "Document with Recipe ID "+ID+" has been deleted");
        return response.toString();
    }

    public String updateRecipe(Recipe recipe) throws ExecutionException, InterruptedException {
        Firestore dbFirestore = FirestoreClient.getFirestore();

        Map<String, Object> docData = new HashMap<>();
        docData.put("id", recipe.getID());
        docData.put("name", recipe.getName());
        docData.put("photoURL", recipe.getPhotoURL());
        docData.put("description", recipe.getDescription());
        docData.put("specific", recipe.getSpecific());
        docData.put("meal", recipe.getMeal());

        ApiFuture<WriteResult> collectionsApiFuture = dbFirestore.collection(COL_NAME).document(recipe.getID()).set(docData);

        for (IngredientList i : getAllIngredientsOfRecipe(recipe.getID())){
            dbFirestore.collection(COL_NAME).document(recipe.getID()).collection(SUB_COL_NAME).document(i.getID()).delete();
        }

        for (int i = 0; i < recipe.getIngredientList().size(); i++) {
            saveIngredientOfList(recipe.getIngredientList().get(i), recipe.getID());
        }

        JsonObject response = new JsonObject();
        response.addProperty("data", collectionsApiFuture.get().getUpdateTime().toString());
        return response.toString();
    }

}
