package ro.devis.recipefinder.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ro.devis.recipefinder.model.Ingredient;
import ro.devis.recipefinder.model.IngredientList;
import ro.devis.recipefinder.model.Recipe;
import ro.devis.recipefinder.service.RecipeService;

import java.util.List;
import java.util.concurrent.ExecutionException;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping("/api/v1/")
public class RecipeController {

    @Autowired
    RecipeService recipeService;

    @GetMapping("/recipe")
    public Recipe getRecipe(@RequestParam String id) throws InterruptedException, ExecutionException {
        return recipeService.getRecipe(id);
    }

    @GetMapping("/recipes")
    public List<Recipe> getAllRecipes() throws InterruptedException, ExecutionException {
        return recipeService.getAllRecipes();
    }

    @PostMapping("/saveRecipe")
    public String saveRecipe(@RequestBody Recipe recipe) throws InterruptedException, ExecutionException {
        return recipeService.saveRecipe(recipe);
    }

    @DeleteMapping("/deleteRecipe")
    public String deleteIngredient(@RequestParam String id) throws ExecutionException, InterruptedException {
        return recipeService.deleteRecipe(id);
    }

    @PutMapping("/updateRecipe")
    public String updateIngredient(@RequestBody Recipe recipe) throws InterruptedException, ExecutionException {
        return recipeService.updateRecipe(recipe);
    }

}
