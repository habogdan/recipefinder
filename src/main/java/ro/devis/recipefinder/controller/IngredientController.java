package ro.devis.recipefinder.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ro.devis.recipefinder.model.Ingredient;
import ro.devis.recipefinder.service.IngredientService;

import java.util.List;
import java.util.concurrent.ExecutionException;


@RestController
@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping("/api/v1/")
public class IngredientController {

    @Autowired
    IngredientService ingredientService;

    @GetMapping("/ingredient")
    public Ingredient getIngredient(@RequestParam String id ) throws InterruptedException, ExecutionException {
        return ingredientService.getIngredient(id);
    }

    @PostMapping("/saveIngredient")
    public String saveIngredient(@RequestBody Ingredient ingredient ) throws InterruptedException, ExecutionException {
        return ingredientService.saveIngredient(ingredient);
    }

    @PutMapping("/updateIngredient")
    public String updateIngredient(@RequestBody Ingredient ingredient) throws InterruptedException, ExecutionException {
        return ingredientService.updateIngredient(ingredient);
    }

    @DeleteMapping("/deleteIngredient")
    public String deleteIngredient(@RequestParam String id){
        return ingredientService.deleteIngredient(id);
    }

    @GetMapping("/ingredients")
    public List<Ingredient> getAllIngredients() throws InterruptedException, ExecutionException {
        return ingredientService.getAllIngredients();
    }
}
