package ro.devis.recipefinder.model;

import com.google.cloud.firestore.annotation.DocumentId;

public class Ingredient {

    @DocumentId
    private String ID;
    private String name;
    private String photoURL;

    public String getID() {
        return ID;
    }

    public void setID(String ID) {
        this.ID = ID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhotoURL() {
        return photoURL;
    }

    public void setPhotoURL(String photoURL) {
        this.photoURL = photoURL;
    }
}
