package ro.devis.recipefinder.model;

import com.google.cloud.firestore.annotation.DocumentId;

import java.util.List;

public class Recipe {

    @DocumentId
    private String ID;
    private String name;
    private String photoURL;
    private String description;
    private String specific;
    private String meal;
    private List<IngredientList> ingredientList;

    public Recipe(){}

    public Recipe(String ID, String name, String photoURL, String description, String specific, String meal, List<IngredientList> ingredientList) {
        this.ID = ID;
        this.name = name;
        this.photoURL = photoURL;
        this.description = description;
        this.specific = specific;
        this.meal = meal;
        this.ingredientList = ingredientList;
    }

    public String getID() {
        return ID;
    }

    public void setID(String ID) {
        this.ID = ID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhotoURL() {
        return photoURL;
    }

    public void setPhotoURL(String photoURL) {
        this.photoURL = photoURL;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getSpecific() {
        return specific;
    }

    public void setSpecific(String specific) {
        this.specific = specific;
    }

    public String getMeal() {
        return meal;
    }

    public void setMeal(String meal) {
        this.meal = meal;
    }

    public List<IngredientList> getIngredientList() {
        return ingredientList;
    }

    public void setIngredientList(List<IngredientList> ingredientList) {
        this.ingredientList = ingredientList;
    }
}
