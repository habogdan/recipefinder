package ro.devis.recipefinder.model;

import com.google.cloud.firestore.annotation.DocumentId;

public class IngredientList {

    @DocumentId
    private String ID;
    private String name;
    private String quantity;

    public IngredientList() {
    }

    public IngredientList(String ID, String name, String quantity) {
        this.ID = ID;
        this.name = name;
        this.quantity = quantity;
    }

    public String getID() {
        return ID;
    }

    public void setID(String ID) {
        this.ID = ID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }
}
